public class App {
    
         public static int[] deleteElementByIndex(int[] arr, int index) {
        if (index >= 0 && index < arr.length) {
            int[] updatedArray = new int[arr.length - 1];
            int k = 0;
            for (int i = 0; i < arr.length; i++) {
                if (i != index) {
                    updatedArray[k] = arr[i];
                    k++;
                }
            }
            return updatedArray;
        } else {
            // Invalid index, return the original array
            return arr;
        }
    }

    public static int[] deleteElementByValue(int[] arr, int value) {
        int index = -1; // Initialize the index of the value to be deleted
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == value) {
                index = i;
                break; // Found the value, exit the loop
            }
        }

        if (index != -1) {
            return deleteElementByIndex(arr, index);
        } else {
            // Value not found, return the original array
            return arr;
        }
    }

    public static void main(String[] args) {
        int[] myArray = {1, 2, 3, 4, 3, 5};
        int valueToDelete = 3;

        System.out.println("Original Array:");
        printArray(myArray);

        int[] updatedArray = deleteElementByValue(myArray, valueToDelete);

        System.out.println("Updated Array:");
        printArray(updatedArray);
    }

    public static void printArray(int[] arr) {
        for (int num : arr) {
            System.out.print(num + " ");
        }
        System.out.println();
    }
}