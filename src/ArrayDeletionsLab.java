public class ArrayDeletionsLab {

    public static int[] deleteElementByIndex(int[] arr, int index) {
        if (index >= 0 && index < arr.length) {
            int[] newArray = new int[arr.length - 1];
            int k = 0;
            for (int i = 0; i < arr.length; i++) {
                if (i != index) {
                    newArray[k] = arr[i];
                    k++;
                }
            }
            return newArray;
        } else {

            return arr;
        }
    }

    public static int[] deleteElementByValue(int[] arr, int value){
        int index = -1; // Initialize the index of the value to be deleted
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == value) {
                index = i;
                break; // Found the value, exit the loop
            }
        }

        if (index != -1) {
            return deleteElementByIndex(arr, index);
        } else {
            // Value not found, return the original array
            return arr;
        }
    }

    public static void main(String[] args) {
        int[] Array = {1, 2, 3, 4, 5};
        int deleteIndex = 2;
        int deleteValue = 5;

        System.out.println("Original Array:");
        printArray(Array);

        int[] newArray = deleteElementByIndex(Array, deleteIndex);

        System.out.println("Array after deleting element at index "+deleteIndex+": ");
        printArray(newArray);

        int[] updatedArray2 = deleteElementByValue(Array, deleteValue);

        System.out.println("Array after deleting element with value " + deleteValue + ":");
        printArray(updatedArray2);

    }

    public static void printArray(int[] arr) {
        for (int num : arr) {
            System.out.print(num + " ");
        }
        System.out.println();
    }
}